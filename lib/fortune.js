var fortuneCookies = [
	"金",
	"木",
	"水",
	"火",
	"土",
];

var loveCookies = [
	"瑜勳",
	"芷綾",
	"佩淑",
	"甜甜",
	"萱萱",
	"今村",
	"鴨鴨",
	"雨青",
	"喵喵",
	"馨筠",
	"繼葳",
	"巍巍",
	"亭文",
	"宛璇",
	"馥君",
	"馥萍",
	"湘涵",
	"昕葳",
	"銀河",
	"怡婷",
	"豫穎",
];

exports.getFortune = function() {
	var idx = Math.floor(Math.random() * fortuneCookies.length);
	return fortuneCookies[idx];
}
exports.getLove = function() {
	var idx = Math.floor(Math.random() * loveCookies.length);
	return loveCookies[idx];
}