var express = require('express');
var handlebars = require('express3-handlebars').create({
	defaultLayout:'main', //Assign the default layout.
	helpers: {
		section: function(name, options){
			if(!this._sections) this._sections = {};
			this._sections[name] = options.fn(this);
			return null;
		}
	}
});

var fortune = require('./lib/fortune.js');

var app = express();

var tours = [
	{ id: 0, name: 'National Taiwan University', ranking: 150 },
	{ id: 1, name: 'The University of Edinburgh', ranking: 23 },
	{ id: 2, name: 'National Taiwan Normal University', ranking: 300 },
]

function getWeatherData(){
	return {
		locations: [
			{
				name: 'Portland',
				forecastUrl: 'http://www.wunderground.com/US/OR/Portland.html',
				iconUrl: 'http://icons-ak.wxug.com/i/c/k/cloudy.gif',
				weather: 'Overcast',
				temp: '54.1 F (12.3 C)',
			},
			{
				name: 'Bend',
				forecastUrl: 'http://www.wunderground.com/US/OR/Bend.html',
				iconUrl: 'http://icons-ak.wxug.com/i/c/k/partlycloudy.gif',
				weather: 'Partyly Cloudy',
				temp: '55.0 F (12.8 C)',
			},
			{
				name: 'Manzanita',
				forecastUrl: 'http://www.wunderground.com/US/OR/Manzanita.html',
				iconUrl: 'http://icons-ak.wxug.com/i/c/k/rain.gif',
				weather: 'Light Rain',
				temp: '55.0 F (12.8 C)',
			},
		],
	};
}

app.engine('handlebars', handlebars.engine);

app.set('view engine', 'handlebars');

app.set('port', process.env.PORT || 3000);

app.use(require('body-parser')());

//The partials setting here should be used by weather.handlebars,
//The partials will be loaded by home.handlebars，so it needs to be ready before using home render.
app.use(function(req, res, next){
	if(!res.locals.partials) res.locals.partials = {};
	res.locals.partials.weather = getWeatherData();
	//console.log(res.locals.partials.weather.locations);
	next();
});

app.get('/', function(req, res){
	res.render('home');
});

app.get('/newsletter', function(req, res){
	res.render('newsletter', { csrf: 'CSRF token goes here' });
});

app.post('/process',  function(req, res){
	console.log('Form (from querystring): ' + req.query.form);
	console.log('CSRF token (from hidden form field): ' + req.body._csrf);
	console.log('Name (from visible form field): ' + req.body.name);
	console.log('Email(from visible form field): ' + req.body.email);
	res.redirect(303, '/about');
});

app.get('/jquerytest', function(req, res){
	res.render('jquerytest', {
		layout: 'test'
	 });
});

app.get('/nursery-rhyme', function(req, res){
	res.render('nursery-rhyme', {
		layout: 'test'
	});
});

app.get('/data/nursery-rhyme', function(req, res){
	res.json({
		animal: 'squirrel',
		bodyPart: 'tail',
		adjective: 'bushy',
		noun: 'heck',
	});
});

app.get('/about', function(req, res){
	//console.log(req.xhr);
	res.render('about', { fortune: fortune.getFortune() });
});

app.get('/january-specials', function(req, res){
	//console.log(req.xhr);
	res.render('january-specials', { layout: 'custom' ,love: fortune.getLove() });
});

app.get('/currency', function(req, res){
	//res.type('text/plain');
	//res.send('This is a test page');
	res.render('currency', {
		layout: 'custom',
		currency: {
			name: 'United States dollars',
			abbrev: 'USD',
		},
		tours: [
			{ name: 'Hood River', price: '$99.95' },
			{ name: 'Oregon Coast', price: '$159.95' },
		],
		specialsUrl: '/january-specials',
		currencies: [ 'USD', 'GBP', 'BTC' ],
	 });
});

app.get('/store', function(req, res){
	//console.log(req.xhr);
	res.render('store', { layout: 'custom' ,love: fortune.getLove() });
});

//According to the request from Client to give different response.
app.get('/api/colleges', function(req, res){
	var toursXml = '<?xml version="1.0" encoding="UTF-8"?><tours>' +
		tours.map(function(p){
			return '<tour ranking="' + p.ranking + '" id = "' + p.id + '">' + p.name + '</tour>';
		}).join('') + '</tours>';
	var toursText = tours.map(function(p){
		return p.id + ': ' + p.name + ' (' + p.price + ') ';
	}).join('\n');
	//console.log(req.headers);
	res.format({
		'application/json': function(){
			console.log('1');
			res.json(tours);
		},
		'application/xml': function(){
			console.log('2');
			res.type('application/xml');
			res.send(toursXml);
		},
		'text/xml': function(){
			console.log('3');
			res.type('text/xml');
			res.send(toursXml);
		},
		'text/plain': function(){
			console.log('4');
			res.type('text/plain');
			res.send(toursText);
		},
		'application/xhtml+xml': function(){
			console.log('5');
			res.type('application/xml');
			res.send(toursXml);
		},
	});
});

app.get('/api/tour/:id', function(req, res){
	var p = tours.some(function(p){ return p.id == req.params.id });
	if( p ) {
		if( req.query.name ) {
			p.name = req.query.name;
		}
		if( req.query.ranking ) {
			p.ranking = req.query.ranking;
		}
		res.json({sucess: true});
	} else {
		res.json({error: 'No such tour exists.'});
	}
});

app.post('/process-contact', function(req, res){
	console.log('Received contact from' + req.body.name + '<' + req.body.email + '>');
	res.redirect(303, '/about');
});

app.use(express.static(__dirname + '/public'));

//Customizing 404 webpage
app.use(function(req, res){
	res.status(404);
	res.render('404');
});
//404 and 500 are currently use to callback the middle ware.
//And according to the different insert parameter to distinguish.

//Customizing 500網頁
app.use(function(err, req, res, next){
	console.error(err.stack);
	res.status(500);
	res.render('500');
});

app.listen(app.get('port'), function(){
	console.log( 'Express started on http://localhost:' +
		app.get('port') + '; press Ctrl-C to teminate.' );
});